Feature: Creation of new User on demoqa.com and validate that new user is created

  Scenario Outline: Create a new account using valid Username and Password
    Given The end point url
    When Create an user with "<username>" and "<password>"
    Then validate the status code
    And response body
    Examples:
      | username     | password     |
      | myTestUser#1 | myTestPass#1 |
      | myTestUser#2 | myTestPass#2 |

  Scenario Outline: Login using username and password and validate the username
    Given go to the url
    When Enter the "<username>" and "<password>"
    And Click on Login button
    Then validate the "<username>" on the Books Dashboard Page
    Examples:
      | username | password |
      | myTestUser#1 | myTestPass#1 |
      | myTestUser#2 | myTestPass#2 |