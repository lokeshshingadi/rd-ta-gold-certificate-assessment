# new feature
# Tags: optional

Feature: Validate the Books on the Page

  Scenario: Fetch all the List of Books and Validate the same on the Page
    Given the api end point url
    When fetch the response from api end point url
    And launch the Books url
    Then validate the authors
    And validate the titles
    And validate the publishers
    