package com.epam.stepdefinations;

import com.epam.containers.BooksValidationContainer;
import com.epam.driverfactory.factory.DriverMangerFactory;
import com.epam.pages.AllBooks;
import com.epam.pojo.Books;
import com.epam.utility.URLs;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.openqa.selenium.WebElement;

import java.util.List;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class BooksValidationSteps {
	BooksValidationContainer booksValidationContainer;
	public BooksValidationSteps(BooksValidationContainer validationContainer)
	{
		booksValidationContainer = validationContainer;
	}
	@Given("the api end point url")
	public void theApiEndPointUrl() {
		baseURI = URLs.fetchBooksEndPointURL;
	}

	@When("fetch the response from api end point url")
	public void fetchTheResponseFromApiEndPointUrl() {
		booksValidationContainer.setResponse(given().when().get());
		booksValidationContainer.setBooks(booksValidationContainer.getResponse().as(Books.class));
		System.out.println(booksValidationContainer.getBooks());
	}

	@And("launch the Books url")
	public void launchTheBooksUrl() {
		booksValidationContainer.setAllBooks();
		booksValidationContainer.getAllBooks().launchWebsite();
		booksValidationContainer.setTitleOfAllBooks(
				booksValidationContainer.getAllBooks()
						.getBooksTitle().stream().map(WebElement::getText)
						.toList());
		booksValidationContainer.setAuthorsOfBooks(booksValidationContainer.getAllBooks().getAuthors().stream().map(WebElement::getText).toList());
		booksValidationContainer.setPublishersOfBooks(booksValidationContainer.getAllBooks().getPublishers().stream().map(WebElement::getText).toList());
	}

	@Then("validate the authors")
	public void validateTheResponseAgainstTheContentOnThePage() {
		assertThat(booksValidationContainer.getAuthorsOfBooks(), equalTo(booksValidationContainer.getBooks().getListOfBookAuthors()));
	}

	@And("validate the titles")
	public void validateTheTitles() {
		assertThat(booksValidationContainer.getTitleOfAllBooks(), equalTo(booksValidationContainer.getBooks().getListOfBookTitle()));
	}

	@And("validate the publishers")
	public void validateThePublishers() {
		assertThat(booksValidationContainer.getPublishersOfBooks(), equalTo(booksValidationContainer.getBooks().getListOfBookPublishers()));
	}
}
