package com.epam.stepdefinations;

import com.epam.containers.LoginValidationContainer;
import com.epam.driverfactory.factory.DriverMangerFactory;
import com.epam.pages.Login;
import com.epam.pages.Profile;
import com.epam.pojo.User;
import com.epam.utility.URLs;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.testng.Assert;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class LoginValidationSteps {
	Response response;
	LoginValidationContainer loginValidationContainer;

	public LoginValidationSteps(LoginValidationContainer loginValidationContainer) {
		this.loginValidationContainer = loginValidationContainer;
	}


	@Given("The end point url")
	public void theEndPointUrl() {
		baseURI = URLs.createUserEndPointURL;
	}

	@When("Create an user with {string} and {string}")
	public void createAnUserWithAnd(String username, String password) {
		loginValidationContainer.setUser(new User(username, password));
		loginValidationContainer.setResponse(given().contentType("application/json").body(loginValidationContainer.getUser()).when().post().thenReturn());
		loginValidationContainer.getResponse().prettyPrint();
	}

	@Then("validate the status code")
	public void validateTheStatusCodeAndResponseBody() {

		loginValidationContainer
				.getResponse()
				.then()
				.statusCode(201);
	}

	@And("response body")
	public void responseBody() {
		loginValidationContainer.getResponse()
				.then()
				.body("username", is(equalTo(loginValidationContainer.getUser().getUserName())));
	}

	@Given("go to the url")
	public void goToTheUrl() {
		loginValidationContainer.setLogin();
		loginValidationContainer.getLogin().launchWebsite();
	}

	@When("Enter the {string} and {string}")
	public void enterTheAnd(String username, String password) {
		loginValidationContainer.getLogin().enterUsername(username);
		loginValidationContainer.getLogin().enterPassword(password);
	}

	@And("Click on Login button")
	public void clickOnLoginButton() {
		loginValidationContainer.setProfile(loginValidationContainer.getLogin().clickLogin());
	}

	@Then("validate the {string} on the Books Dashboard Page")
	public void validateTheOnTheBooksDashboardPage(String username) {
		Assert.assertEquals(loginValidationContainer.getProfile().getUserName(), username);
	}
	
}
