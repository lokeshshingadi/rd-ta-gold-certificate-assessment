package com.epam.pojo;

import com.fasterxml.jackson.annotation.*;

import java.util.LinkedHashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"isbn",
		"title",
		"subTitle",
		"author",
		"publish_date",
		"publisher",
		"pages",
		"description",
		"website"
})
public class Book {

	@JsonProperty("isbn")
	private String isbn;
	@JsonProperty("title")
	private String title;
	@JsonProperty("subTitle")
	private String subTitle;
	@JsonProperty("author")
	private String author;
	@JsonProperty("publish_date")
	private String publishDate;
	@JsonProperty("publisher")
	private String publisher;
	@JsonProperty("pages")
	private Integer pages;
	@JsonProperty("description")
	private String description;
	@JsonProperty("website")
	private String website;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

	@JsonProperty("isbn")
	public String getIsbn() {
		return isbn;
	}

	@JsonProperty("isbn")
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("subTitle")
	public String getSubTitle() {
		return subTitle;
	}

	@JsonProperty("subTitle")
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	@JsonProperty("author")
	public String getAuthor() {
		return author;
	}

	@JsonProperty("author")
	public void setAuthor(String author) {
		this.author = author;
	}

	@JsonProperty("publish_date")
	public String getPublishDate() {
		return publishDate;
	}

	@JsonProperty("publish_date")
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	@JsonProperty("publisher")
	public String getPublisher() {
		return publisher;
	}

	@JsonProperty("publisher")
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	@JsonProperty("pages")
	public Integer getPages() {
		return pages;
	}

	@JsonProperty("pages")
	public void setPages(Integer pages) {
		this.pages = pages;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("website")
	public String getWebsite() {
		return website;
	}

	@JsonProperty("website")
	public void setWebsite(String website) {
		this.website = website;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "\n\t{" +
				"\n\tisbn='" + isbn + '\'' +
				",\n\ttitle='" + title + '\'' +
				",\n\tsubTitle='" + subTitle + '\'' +
				",\n\tauthor='" + author + '\'' +
				",\n\tpublishDate='" + publishDate + '\'' +
				",\n\tpublisher='" + publisher + '\'' +
				",\n\tpages=" + pages +
				",\n\tescription='" + description + '\'' +
				",\n\twebsite='" + website + '\'' +
				"\n\t}";
	}
}