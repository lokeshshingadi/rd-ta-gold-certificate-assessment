package com.epam.pojo;

import com.fasterxml.jackson.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"books"
})

public class Books {

	@JsonProperty("books")
	private List<Book> books;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

	@JsonProperty("books")
	public List<Book> getBooks() {
		return books;
	}

	@JsonProperty("books")
	public void setBooks(List<Book> books) {
		this.books = books;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	public List<String> getListOfBookTitle()
	{
		return books.stream().map(book -> book.getTitle()).toList();
	}
	public List<String> getListOfBookAuthors()
	{
		return books.stream().map(book -> book.getAuthor()).toList();
	}
	public List<String> getListOfBookPublishers()
	{
		return books.stream().map(book -> book.getPublisher()).toList();
	}

	@Override
	public String toString() {
		return "{\n" +
				"books=" + books +
				"\n}";
	}
}
