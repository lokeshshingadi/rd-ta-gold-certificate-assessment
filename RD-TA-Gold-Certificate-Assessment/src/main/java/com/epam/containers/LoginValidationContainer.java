package com.epam.containers;

import com.epam.driverfactory.factory.DriverMangerFactory;
import com.epam.pages.Login;
import com.epam.pages.Profile;
import com.epam.pojo.User;
import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeAll;
import io.restassured.response.Response;
import org.openqa.selenium.remote.RemoteWebDriver;

public class LoginValidationContainer {
	private RemoteWebDriver webDriver;
	private Response response;
	private User user;
	private Login login;
	private Profile profile;
	private RemoteWebDriver getWebDriver() {
		if(webDriver==null)
			setWebDriver();
		return webDriver;
	}
	public void setWebDriver() {
		if (webDriver == null)
			webDriver = DriverMangerFactory.getDriverManager().getWebDriver();
	}
	@After
	public void closeWebDriver() {
		webDriver.quit();
		webDriver = null;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		if(this.response==null)
			this.response = response;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		if(this.user==null)
			this.user = user;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin() {
			this.login = new Login(getWebDriver());
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		if(this.profile == null)
			this.profile = profile;
	}
}
