package com.epam.containers;

import com.epam.driverfactory.factory.DriverMangerFactory;
import com.epam.pages.AllBooks;
import com.epam.pojo.Books;
import io.cucumber.java.After;
import io.restassured.response.Response;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

public class BooksValidationContainer {
	private Response response;
	private List<String> titleOfAllBooks;
	private List<String> authorsOfBooks;
	private List<String> publishersOfBooks;
	private Books books;
	private AllBooks allBooks;
	private RemoteWebDriver webDriver;

	private RemoteWebDriver getWebDriver() {
		if(webDriver==null)
			setWebDriver();
		return webDriver;
	}
	public void setWebDriver() {
		if (webDriver == null)
			webDriver = DriverMangerFactory.getDriverManager().getWebDriver();
	}
	@After
	public void closeWebDriver() {
		webDriver.quit();
		webDriver = null;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public List<String> getTitleOfAllBooks() {
		return titleOfAllBooks;
	}

	public void setTitleOfAllBooks(List<String> titleOfAllBooks) {
		this.titleOfAllBooks = titleOfAllBooks;
	}

	public List<String> getAuthorsOfBooks() {
		return authorsOfBooks;
	}

	public void setAuthorsOfBooks(List<String> authorsOfBooks) {
		this.authorsOfBooks = authorsOfBooks;
	}

	public List<String> getPublishersOfBooks() {
		return publishersOfBooks;
	}

	public void setPublishersOfBooks(List<String> publishersOfBooks) {
		this.publishersOfBooks = publishersOfBooks;
	}

	public Books getBooks() {
		return books;
	}

	public void setBooks(Books books) {
		this.books = books;
	}

	public AllBooks getAllBooks() {
		return allBooks;
	}

	public void setAllBooks() {
		this.allBooks = new AllBooks(getWebDriver());
	}
}
