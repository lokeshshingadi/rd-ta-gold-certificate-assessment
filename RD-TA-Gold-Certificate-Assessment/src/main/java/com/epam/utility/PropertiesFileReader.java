package com.epam.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFileReader {
	private PropertiesFileReader() {
	}

	public static Properties readPropertiesFile(String filePath) {
		Properties properties;
		try (FileInputStream fileInputStream = new FileInputStream(filePath)) {
			properties = new Properties();
			properties.load(fileInputStream);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return properties;
	}
}
