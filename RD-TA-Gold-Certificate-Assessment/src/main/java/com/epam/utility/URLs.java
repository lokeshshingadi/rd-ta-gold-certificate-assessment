package com.epam.utility;

import java.util.Properties;

public class URLs {
	private static final Properties properties =
			PropertiesFileReader.readPropertiesFile("src/main/resources/urls.properties");
	public static final String createUserEndPointURL = properties.getProperty("createUserEndPointURL");
	public static final String loginURL = properties.getProperty("loginURL");
	public static final String fetchBooksEndPointURL = properties.getProperty("fetchBooksEndPointURL");
	public static final String booksURL = properties.getProperty("booksURL");
	
	
}
