package com.epam.driverfactory.drivers;

import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverManager extends DriverManger{
	public FirefoxDriverManager(){
		webDriver = new FirefoxDriver();
	}
}
