package com.epam.driverfactory.drivers;

import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class DriverManger {
	protected RemoteWebDriver webDriver;
	public RemoteWebDriver getWebDriver()
	{
		return webDriver;
	}
}
