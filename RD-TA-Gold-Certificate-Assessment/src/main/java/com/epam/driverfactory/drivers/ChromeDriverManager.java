package com.epam.driverfactory.drivers;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverManager extends DriverManger{
	public ChromeDriverManager()
	{
		webDriver = new ChromeDriver();
		webDriver.manage().window().maximize();
	}
}
