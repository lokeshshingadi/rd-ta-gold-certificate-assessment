package com.epam.driverfactory.drivers;

import org.openqa.selenium.edge.EdgeDriver;

public class EdgeDriverManager extends DriverManger{
	public EdgeDriverManager()
	{
		webDriver = new EdgeDriver();
		webDriver.manage().window().maximize();
	}
}
