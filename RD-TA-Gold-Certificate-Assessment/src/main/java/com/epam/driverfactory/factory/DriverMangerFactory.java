package com.epam.driverfactory.factory;

import com.epam.driverfactory.drivers.ChromeDriverManager;
import com.epam.driverfactory.drivers.DriverManger;
import com.epam.driverfactory.drivers.EdgeDriverManager;
import com.epam.driverfactory.drivers.FirefoxDriverManager;
import com.epam.exceptions.InvalidBrowser;
import com.epam.utility.PropertiesFileReader;

import java.util.Properties;

public class DriverMangerFactory {
	private DriverMangerFactory() {

	}

	static final String browserPropertiesFilePath = "src/main/resources/browser.properties";

	public static DriverManger getDriverManager() {
		DriverManger driverManger;
		Properties browser = PropertiesFileReader.readPropertiesFile(browserPropertiesFilePath);
		switch (browser.getProperty("browserName").toUpperCase()) {
			case "CHROME" -> driverManger = new ChromeDriverManager();
			case "EDGE" -> driverManger = new EdgeDriverManager();
			case "FIREFOX" -> driverManger = new FirefoxDriverManager();
			default -> throw new InvalidBrowser("Specified Browser Not found");
		}
		return driverManger;
	}
}
