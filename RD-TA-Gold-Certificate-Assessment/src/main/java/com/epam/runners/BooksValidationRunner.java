package com.epam.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(glue = {"com.epam.stepdefinations"},
		features = {"src/test/resources/BooksValidation.feature"}
,plugin = {"pretty","json:target/Reports/BooksValidation/reports.json","html:target/Reports/BooksValidation/reports.html","testng:target/Reports/BooksValidation/reports.xml","com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}
,publish = true)
public class BooksValidationRunner extends AbstractTestNGCucumberTests {
}
