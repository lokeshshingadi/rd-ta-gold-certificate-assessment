package com.epam.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(glue = {"com.epam.stepdefinations"},
		features = {"src/test/resources/LoginValidation.feature"}
,plugin = {"pretty","json:target/Reports/LoginValidation/reports.json","html:target/Reports/LoginValidation/reports.html","testng:target/Reports/LoginValidation/reports.xml","com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}
,publish = true)
public class LoginValidationRunner extends AbstractTestNGCucumberTests {
}
