package com.epam.exceptions;

public class InvalidBrowser extends IllegalArgumentException {
	static final String UNSUPPORTED_BROWSER = """
			Browsers Supported
			1.CHROME
			2.EDGE
			3.FIREFOX
									
			Go to src/main/resources/browser.properties
			and kindly change your preference to any supported Browser""";

	public InvalidBrowser(String message) {
		super(message + UNSUPPORTED_BROWSER);
	}
}
