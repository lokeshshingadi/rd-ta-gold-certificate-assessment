package com.epam.pages;

import com.epam.utility.URLs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class AllBooks extends BasePage{
	private static final Logger logger = LogManager.getLogger(AllBooks.class);
	private static final String BASE_URL = URLs.booksURL;

	public AllBooks(RemoteWebDriver driver) {
		super(driver);
	}
	public void launchWebsite() {
		this.launchWebsite(BASE_URL);
		logger.info("website launced");
	}
	@FindBy(xpath = "//div[@role='rowgroup']/div/div[2]/div/span/a")
	private List<WebElement> booksTitle;
	
	@FindBy(xpath = "//div[@role='rowgroup']/div/div[2]/div/parent::div/following-sibling::div[1]")
	private List<WebElement> authors;
	@FindBy(xpath = "//div[@role='rowgroup']/div/div[2]/div/parent::div/following-sibling::div[2]")
	private List<WebElement> publishers;

	public List<WebElement> getBooksTitle() {
		logger.info("getting titles");
		return booksTitle;
	}

	public List<WebElement> getAuthors() {
		logger.info("getting authors");
		return authors;
	}

	public List<WebElement> getPublishers() {
		logger.info("getting publishers");
		return publishers;
	}
}
