package com.epam.pages;

import com.epam.utility.URLs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import java.util.Properties;

public class Login extends BasePage{
	private static final Logger logger = LogManager.getLogger(Login.class);

	private static final String BASE_URL = URLs.loginURL;

	public Login(RemoteWebDriver driver) {
		super(driver);
	}
	public void launchWebsite() {
		this.launchWebsite(BASE_URL);
		logger.info("login website launch");
	}
	
	@FindBy(id = "userName")
	private WebElement username;
	@FindBy(id = "password")
	private WebElement password;
	@FindBy(id = "login")
	private WebElement loginButton;
	@FindBy(id = "newUser")
	private WebElement newUserButton;
	public void enterUsername(String username)
	{
		logger.info("entering username");
		this.username.sendKeys(username);
	}
	public void enterPassword(String password)
	{
		logger.info("entering password");
		this.password.sendKeys(password);
	}
	public Profile clickLogin()
	{
		getJavascriptExecutor().executeScript("arguments[0].scrollIntoView();", loginButton);
		logger.info("clicking login");
		loginButton.click();
		return new Profile(getWebDriver());
	}
	public void clickNewUser()
	{
		newUserButton.click();
	}
	
}
