package com.epam.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Profile extends BasePage{

	public Profile(RemoteWebDriver driver) {
		super(driver);
	}
	@FindBy(id = "userName-value")
	private WebElement userName;
	@FindBy(xpath = "//button[@id='submit'][normalize-space(.)='Log out']")
	private WebElement logout;
	@FindBy(xpath = "//button[@id='submit'][normalize-space(.)='Delete Account']")
	private WebElement deleteAccount;
	@FindBy(xpath = "//button[@id='submit'][normalize-space(.)='Delete All Books']")
	private WebElement deleteAllBooks;
	@FindBy(id = "gotoStore")
	private WebElement goToBookStore;
	
	public String getUserName()
	{
		getWait().until(ExpectedConditions.visibilityOf(userName));
		return userName.getText();
	}
	public void logout()
	{
		logout.click();
	}
	public void deleteAccount()
	{
		deleteAccount.click();
	}
	public void deleteAllBooks()
	{
		deleteAllBooks.click();
	}
	public void goToBookStore()
	{
		goToBookStore.click();
	}
	
}
