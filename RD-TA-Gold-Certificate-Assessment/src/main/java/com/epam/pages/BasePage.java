package com.epam.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class BasePage {
	private RemoteWebDriver webDriver;
	private JavascriptExecutor javascriptExecutor;
	private WebDriverWait wait;

	public BasePage(RemoteWebDriver driver)
	{
		webDriver = driver;
		webDriver.manage().window().maximize();
		javascriptExecutor = (JavascriptExecutor) webDriver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		PageFactory.initElements(webDriver,this);
	}
	protected RemoteWebDriver getWebDriver()
	{
		return webDriver;
	}
	protected JavascriptExecutor getJavascriptExecutor() {
		return javascriptExecutor;
	}
	protected void launchWebsite(String URL)
	{
		webDriver.get(URL);
	}
	protected WebDriverWait getWait(){return wait;}
	public String getCurrentUrl()
	{
		return webDriver.getCurrentUrl();
	}
	
}
